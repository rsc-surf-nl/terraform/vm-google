output "project_id" {
  value = var.google_project
}

output "id" {
  value = google_compute_instance.default.instance_id
}

output "ip" {
  value = google_compute_instance.default.network_interface.0.access_config.0.nat_ip
}

output "instance_user" {
  value = var.instance_user
}

output "private_key" {
  value     = tls_private_key.private_key.private_key_pem
  sensitive = true
}

output "public_key" {
  value = tls_private_key.private_key.public_key_openssh
}

output "ssh_token_base64" {
  value     = base64encode("{\"username\":\"${var.workspace_id}}\",\"ip\":\"${google_compute_instance.default.network_interface.0.access_config.0.nat_ip}\",\"private_key_base64\":\"${base64encode(tls_private_key.private_key.private_key_pem)}\"}")
  sensitive = true
}
