resource "google_compute_network" "vpc_network" {
  count                   = local.external_network != null ? 0 : 1 # Only create a network if no explicit external network has been assigned
  name                    = "src-${var.workspace_id}-vpc"
  auto_create_subnetworks = false
  mtu                     = 1500
  # The default MTU size for google VPC is 1460 which might lead to incompatibility in certain cases like with a docker network with default MTU of 1500.
}

resource "google_compute_subnetwork" "subnet" {
  count         = local.external_network != null ? 0 : 1 # Only create a network if no explicit external network has been assigned
  name          = "src-${var.workspace_id}-subnet"
  ip_cidr_range = var.cidr
  region        = var.region
  network       = google_compute_network.vpc_network[0].id
}

resource "google_compute_firewall" "rules" {
  count         = length(local.security_group_rules)
  name          = "src-${var.workspace_id}-secgroup-${count.index}"
  network       = local.external_network != null ? local.external_network.network_id.value : google_compute_network.vpc_network[0].id
  direction     = split(" ", local.security_group_rules[count.index])[0] == "in" ? "INGRESS" : "EGRESS"
  source_ranges = split(" ", local.security_group_rules[count.index])[0] == "in" ? [split(" ", local.security_group_rules[count.index])[4]] : []
  priority      = 1000 + count.index
  allow {
    protocol = split(" ", local.security_group_rules[count.index])[1]
    ports = [format(
      "%d-%d",
      split(" ", local.security_group_rules[count.index])[2],
      split(" ", local.security_group_rules[count.index])[3],
      )
    ]
  }

  target_tags = split(" ", local.security_group_rules[count.index])[0] == "in" ? ["src-${var.workspace_id}"] : []
}
