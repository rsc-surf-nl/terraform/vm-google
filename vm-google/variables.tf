# Variables used for resource configuration
variable "instance_name" {}
variable "instance_user" {}
variable "image_name" {}
variable "flavor_name" {}
variable "boot_volume_size" {
  type    = number
  default = 15
}
variable "boot_volume_type" {
  default = "pd-standard"
}
variable "region" {}
variable "zone" {}
variable "external_volumes" {
  default = "[]"
}
variable "external_floating_ips" {
  default = "[]"
}
variable "cidr" {
  type    = string
  default = "10.10.10.0/24"
}
variable "security_group_rules" {
  type = list(string)
}
variable "additional_security_group_rules" {
  type    = list(string)
  default = []
}
variable "config_security_group_rules" {
  type    = list(string)
  default = []
}

variable "external_networks" {
  type    = string
  default = "[]"
}

# Google-specific variables
variable "google_project" {}
variable "google_credentials" {}
