# MB : https://registry.terraform.io/providers/hashicorp/google/latest/docs/guides/provider_reference
provider "google" {
  region      = var.region
  project     = var.google_project
  credentials = base64decode("${var.google_credentials}")
}
