
resource "google_project_service" "service" {
  for_each = toset([
    "compute.googleapis.com",
    "oslogin.googleapis.com",
    "cloudbilling.googleapis.com"
  ])

  service = each.key

  project            = var.google_project
  disable_on_destroy = false
}
