resource "tls_private_key" "private_key" {
  algorithm = "RSA"
  rsa_bits  = 4096
}

resource "random_password" "password" {
  length           = 24
  special          = true
  override_special = "_%@)"
}

resource "random_id" "instance-id" {
  byte_length = 8
  prefix      = "src-"
}

locals {
  external-volumes     = tolist(jsondecode(var.external_volumes))
  external-floating-ip = length(tolist(jsondecode(var.external_floating_ips))) != 0 ? tolist(jsondecode(var.external_floating_ips))[0].address.value : null
  security_group_rules = distinct(concat(var.security_group_rules, var.config_security_group_rules, var.additional_security_group_rules))
  external_network     = length(tolist(jsondecode(var.external_networks))) != 0 ? tolist(jsondecode(var.external_networks))[0] : null
}

resource "google_compute_instance" "default" {
  depends_on   = [google_project_service.service]
  project      = var.google_project
  zone         = var.zone
  name         = random_id.instance-id.hex
  machine_type = var.flavor_name
  tags         = ["src-${var.workspace_id}"]
  metadata = {
    ssh-keys           = "${var.instance_user}:${replace(tls_private_key.private_key.public_key_openssh, "\n", "")}"
    admin_pass         = random_password.password.result,
    workspace_id       = var.workspace_id
    subscription       = var.subscription
    application_type   = var.application_type
    resource_type      = var.resource_type
    cloud_type         = var.cloud_type
    subscription_group = var.subscription_group
  }

  # TODO: how do we tag this boot disk?
  boot_disk {
    initialize_params {
      image = var.image_name
      size  = var.boot_volume_size
      type  = var.boot_volume_type
    }
  }

  # This is necessary when there are any attached disks
  lifecycle {
    ignore_changes = [attached_disk]
  }

  network_interface {
    subnetwork = local.external_network != null ? local.external_network.subnet_id.value : google_compute_subnetwork.subnet[0].id
    access_config {
      nat_ip = local.external-floating-ip
    }
  }
}

resource "google_compute_attached_disk" "default" {
  count    = length(local.external-volumes)
  disk     = local.external-volumes[count.index].id.value
  instance = google_compute_instance.default.id
}
